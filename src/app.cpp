#include <app.hpp>

#include "services/physicsservice.hpp"
#include "services/renderservice.hpp"
#include <entities.hpp>
#include <filesystem>
#include <glad/glad.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glprogram.hpp>
#include <glshader.hpp>
#include <imgui.h>

void App::OnInit()
{
    glClearColor(0.56f, 0.7f, 0.67f, 1.0f);
    glEnable(GL_DEPTH_TEST);

    _program = std::unique_ptr<GlProgram>(new GlProgram());
    _physics = std::make_unique<PhysicsService>();
    _renderer = std::make_unique<RenderService>(std::filesystem::current_path() / "assets");

    _program->attach(GLSL_VERTEX_SHADER(
        layout(location = 0) in vec3 aPos;
        layout(location = 1) in vec3 aColor;

        out vec3 Color;

        uniform mat4 mvp;

        void main() {
            gl_Position = mvp * vec4(aPos, 1.0f);
            Color = aColor;
        }));

    _program->attach(GLSL_FRAGMENT_SHADER(
        out vec4 FragColor;

        in vec3 Color;

        void main() {
            FragColor = vec4(Color.rgb, 1.0);
        }));

    _program->link();

    SetupScene();
}

void App::SetupScene()
{
    AddObject("floor.glb", 0.0f, glm::vec3(35.0f, 35.0f, 0.4f), glm::vec3(0.0f, 15.0f, -3.0f));

    auto a = AddBall("ball.glb", 0.01f, 1.0f, glm::vec3(0.0f, 0.0f, 12.0f));
    _physics->ApplyForce(_registry.get<PhysicsComponent>(a), glm::vec3(0.0f, 1.0f, 0.3f), glm::vec3(0.0f, 1.0f, 0.5f));
}

entt::entity App::AddObject(
    const std::string &model,
    float mass,
    const glm::vec3 &size,
    const glm::vec3 &startPos)
{
    auto entity = _registry.create();

    auto component = _physics->AddCube(mass, size, startPos);

    auto mesh = _renderer->LoadMesh(model);

    _registry.emplace<PhysicsComponent>(entity, component);
    _registry.emplace<MeshComponent>(entity, mesh);

    return entity;
}

entt::entity App::AddBall(
    const std::string &model,
    float mass,
    float radius,
    const glm::vec3 &startPos)
{
    auto entity = _registry.create();

    auto component = _physics->AddSphere(mass, radius, startPos);

    auto mesh = _renderer->LoadMesh(model);

    _registry.emplace<PhysicsComponent>(entity, component);
    _registry.emplace<MeshComponent>(entity, mesh);

    return entity;
}

void App::OnResize(
    int width,
    int height)
{
    if (height < 1) height = 1;

    glViewport(0, 0, width, height);

    _projection = glm::perspective(glm::radians<float>(90), float(width) / float(height), 0.1f, 1000.0f);
}

void App::OnFrame(
    std::chrono::nanoseconds diff)
{
    _physics->Step(diff);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    auto viewMatrix = glm::lookAt(
        glm::vec3(10.0f, -10.0f, 15.0f),
        glm::vec3(0.0f, 0.0f, 0.0f),
        glm::vec3(0.0f, 0.0f, 1.0f));

    auto objects = _registry.view<PhysicsComponent, MeshComponent>();
    for (auto entity : objects)
    {
        auto &body = objects.get<PhysicsComponent>(entity);
        auto &mesh = objects.get<MeshComponent>(entity);

        auto mat = _physics->GetMatrix(body);
        _renderer->RenderMesh(mesh, _projection * viewMatrix * mat);
    }

    static bool showPhysicsDebugMode = true;

    if (showPhysicsDebugMode)
    {
        _program->use();
        _program->setUniformMatrix("mvp", _projection * viewMatrix);

        VertexArray vertexAndColorBuffer;

        _physics->RenderDebug(vertexAndColorBuffer);

        vertexAndColorBuffer.upload();

        glDisable(GL_DEPTH_TEST);
        vertexAndColorBuffer.render(RenderModes::Lines, 0);
        glEnable(GL_DEPTH_TEST);
    }

    ImGui::Begin("Debug");
    ImGui::Checkbox("Enabled debug draw", &showPhysicsDebugMode);
    if (ImGui::Button("Restart"))
    {
        _physics = std::make_unique<PhysicsService>();

        SetupScene();
    }
    ImGui::End();
}

void App::OnExit()
{
    _program = nullptr;
    _physics = nullptr;
    _renderer = nullptr;
}
