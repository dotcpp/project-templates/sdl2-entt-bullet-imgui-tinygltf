#ifndef RENDERSERVICE_H
#define RENDERSERVICE_H

#include <filesystem>
#include <glad/glad.h>
#include <glprogram.hpp>
#include <irenderservice.hpp>
#include <map>
#include <tuple>
#include <vector>

#define TINYGLTF_NOEXCEPTION
#include <tiny_gltf.h>

struct LoadedAsset
{
    LoadedAsset() = default;
    LoadedAsset(const LoadedAsset &) = default;
    LoadedAsset(LoadedAsset &&) noexcept = default;
    ~LoadedAsset() = default;
    LoadedAsset &operator=(const LoadedAsset &) = default;
    LoadedAsset &operator=(LoadedAsset &&) noexcept = default;

    std::filesystem::path path;
    std::string nodeName;
    GLuint vao;
};

class RenderService : public IRenderService
{
public:
    explicit RenderService(
        const std::filesystem::path &assetRoot);

    virtual ~RenderService();

    virtual MeshComponent LoadMesh(
        const std::string &modelPath,
        const std::string &name);

    virtual void RenderMesh(
        const MeshComponent &mesh,
        const glm::mat4 &matrix);

private:
    std::unique_ptr<GlProgram> _program;
    std::filesystem::path _assetRoot;
    std::map<std::filesystem::path, tinygltf::Model> _assetPathMap;
    std::vector<LoadedAsset> _loadedAssets;

    static MeshComponent _emptyMeshComponent;

    int NodeIndexFromName(
        const tinygltf::Model &model,
        const std::string &name);
};

#endif // RENDERSERVICE_H
