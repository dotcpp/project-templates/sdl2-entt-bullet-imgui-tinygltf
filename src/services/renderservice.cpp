#include "renderservice.hpp"
#include <fmt/format.h>
#include <iostream>

#define TINYGLTF_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#define TINYGLTF_NOEXCEPTION
#include <tiny_gltf.h>

void CheckErrors(std::string desc)
{
    GLenum e = glGetError();
    if (e != GL_NO_ERROR)
    {
        fprintf(stderr, "OpenGL error in \"%s\": %d (%d)\n", desc.c_str(), e, e);
        exit(20);
    }
}

#define BUFFER_OFFSET(i) ((void *)(i))

void bindMesh(
    std::map<size_t, GLuint> &vbos,
    const tinygltf::Model &model,
    const tinygltf::Mesh &mesh)
{
    for (size_t i = 0; i < mesh.primitives.size(); ++i)
    {
        tinygltf::Primitive primitive = mesh.primitives[i];
        tinygltf::Accessor indexAccessor = model.accessors[primitive.indices];

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbos[indexAccessor.bufferView]);

        for (auto &attrib : primitive.attributes)
        {
            const auto &accessor = model.accessors[attrib.second];
            int byteStride = accessor.ByteStride(model.bufferViews[accessor.bufferView]);
            glBindBuffer(GL_ARRAY_BUFFER, vbos[accessor.bufferView]);

            int size = 1;
            if (accessor.type == TINYGLTF_TYPE_SCALAR)
            {
                size = 1;
            }
            else if (accessor.type == TINYGLTF_TYPE_VEC2)
            {
                size = 2;
            }
            else if (accessor.type == TINYGLTF_TYPE_VEC3)
            {
                size = 3;
            }
            else if (accessor.type == TINYGLTF_TYPE_VEC4)
            {
                size = 4;
            }
            else
            {
                assert(0);
            }

            int vaa = -1;
            if (attrib.first.compare("POSITION") == 0) vaa = 0;
            if (attrib.first.compare("COLOR_0") == 0) vaa = 1;
            if (attrib.first.compare("TEXCOORD_0") == 0) vaa = 2;
            if (vaa > -1)
            {
                glVertexAttribPointer(
                    vaa,
                    size,
                    accessor.componentType,
                    accessor.normalized ? GL_TRUE : GL_FALSE,
                    byteStride,
                    BUFFER_OFFSET(accessor.byteOffset));
                glEnableVertexAttribArray(vaa);
            }
            else
            {
                std::cout << "vaa missing: " << attrib.first << std::endl;
            }
        }

        if (model.textures.size() > 0)
        {
            // fixme: Use material's baseColor
            const tinygltf::Texture &tex = model.textures[0];

            if (tex.source > -1)
            {
                GLuint texid;
                glGenTextures(1, &texid);

                const tinygltf::Image &image = model.images[tex.source];

                glBindTexture(GL_TEXTURE_2D, texid);
                glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

                GLenum format = GL_RGBA;

                if (image.component == 1)
                {
                    format = GL_RED;
                }
                else if (image.component == 2)
                {
                    format = GL_RG;
                }
                else if (image.component == 3)
                {
                    format = GL_RGB;
                }
                else
                {
                    // ???
                }

                GLenum type = GL_UNSIGNED_BYTE;
                if (image.bits == 8)
                {
                    // ok
                }
                else if (image.bits == 16)
                {
                    type = GL_UNSIGNED_SHORT;
                }
                else
                {
                    // ???
                }

                glTexImage2D(
                    GL_TEXTURE_2D,
                    0,
                    GL_RGBA,
                    image.width,
                    image.height,
                    0,
                    format,
                    type,
                    &image.image.at(0));
            }
        }
    }
}

// bind models
void bindModelNodes(
    std::map<size_t, GLuint> &vbos,
    const tinygltf::Model &model,
    const tinygltf::Node &node)
{
    if ((node.mesh >= 0) && (static_cast<size_t>(node.mesh) < model.meshes.size()))
    {
        bindMesh(vbos, model, model.meshes[node.mesh]);
    }

    for (auto child : node.children)
    {
        bindModelNodes(vbos, model, model.nodes[child]);
    }
}

GLuint bindModel(
    const tinygltf::Model &model,
    int nodeIndex = -1)
{
    std::map<size_t, GLuint> vbos;
    GLuint vao;

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    for (size_t i = 0; i < model.bufferViews.size(); ++i)
    {
        const tinygltf::BufferView &bufferView = model.bufferViews[i];

        if (bufferView.target == 0)
        {
            // TODO impl drawarrays
            std::cout << "WARN: bufferView.target is zero" << std::endl;

            continue; // Unsupported bufferView.

            /*
              From spec2.0 readme:
              https://github.com/KhronosGroup/glTF/tree/master/specification/2.0
                       ... drawArrays function should be used with a count equal to
              the count            property of any of the accessors referenced by the
              attributes            property            (they are all equal for a given
              primitive).
            */
        }

        const auto &buffer = model.buffers[bufferView.buffer];

        GLuint vbo;
        glGenBuffers(1, &vbo);
        vbos[i] = vbo;
        glBindBuffer(bufferView.target, vbo);

        glBufferData(
            bufferView.target,
            bufferView.byteLength,
            &buffer.data.at(0) + bufferView.byteOffset,
            GL_STATIC_DRAW);
    }

    if (nodeIndex == -1)
    {
        for (auto &node : model.nodes)
        {
            bindModelNodes(vbos, model, node);
        }
    }
    else
    {
        bindModelNodes(vbos, model, model.nodes[nodeIndex]);
    }

    glBindVertexArray(0);
    // cleanup vbos
    for (size_t i = 0; i < vbos.size(); ++i)
    {
        glDeleteBuffers(1, &vbos[i]);
    }

    return vao;
}

void drawMesh(
    const tinygltf::Model &model,
    const tinygltf::Mesh &mesh)
{
    for (auto &primitive : mesh.primitives)
    {
        tinygltf::Accessor indexAccessor = model.accessors[primitive.indices];

        int mode = -1;
        if (primitive.mode == TINYGLTF_MODE_TRIANGLES)
        {
            mode = GL_TRIANGLES;
        }
        else if (primitive.mode == TINYGLTF_MODE_TRIANGLE_STRIP)
        {
            mode = GL_TRIANGLE_STRIP;
        }
        else if (primitive.mode == TINYGLTF_MODE_TRIANGLE_FAN)
        {
            mode = GL_TRIANGLE_FAN;
        }
        else if (primitive.mode == TINYGLTF_MODE_POINTS)
        {
            mode = GL_POINTS;
        }
        else if (primitive.mode == TINYGLTF_MODE_LINE)
        {
            mode = GL_LINES;
        }
        else if (primitive.mode == TINYGLTF_MODE_LINE_LOOP)
        {
            mode = GL_LINE_LOOP;
        }
        else
        {
            assert(0);
        }

        glDrawElements(
            mode,
            indexAccessor.count,
            indexAccessor.componentType,
            BUFFER_OFFSET(indexAccessor.byteOffset));
    }
}

// recursively draw node and children nodes of model
void drawModelNodes(
    const tinygltf::Model &model,
    const tinygltf::Node &node)
{
    if ((node.mesh >= 0) && (static_cast<size_t>(node.mesh) < model.meshes.size()))
    {
        auto &mesh = model.meshes[node.mesh];
        drawMesh(model, mesh);
    }

    for (auto child : node.children)
    {
        drawModelNodes(model, model.nodes[child]);
    }
}

void drawModel(
    GLuint vao,
    const tinygltf::Model &model)
{
    glBindVertexArray(vao);

    for (auto &node : model.nodes)
    {
        drawModelNodes(model, node);
    }

    glBindVertexArray(0);
}

void dbgModel(
    const tinygltf::Model &model)
{
    for (auto &mesh : model.meshes)
    {
        fmt::print("mesh : {}\n", mesh.name);
        for (auto &primitive : mesh.primitives)
        {
            const tinygltf::Accessor &indexAccessor =
                model.accessors[primitive.indices];

            fmt::print("  indexaccessor: count {}, type {}\n", indexAccessor.count, indexAccessor.componentType);

            if (primitive.material < model.materials.size())
            {
                const tinygltf::Material &mat = model.materials[primitive.material];
                for (auto &mats : mat.values)
                {
                    fmt::print("  mat : {} = {}{}\n", mats.first, mats.second.string_value, mats.second.number_value);
                }
            }

            for (auto &image : model.images)
            {
                fmt::print("  image name : {}\n", image.uri);
                fmt::print("    size : {}\n", image.image.size());
                fmt::print("    w/h : {}/{}\n", image.width, image.height);
            }

            fmt::print("  indices  : {}\n", primitive.indices);
            fmt::print("  mode     : ({})\n", primitive.mode);

            for (auto &attrib : primitive.attributes)
            {
                fmt::print("  attribute : {}\n", attrib.first);
            }
        }
    }
}

MeshComponent RenderService::_emptyMeshComponent;

RenderService::RenderService(
    const std::filesystem::path &assetRoot)
    : _program(std::unique_ptr<GlProgram>(new GlProgram())),
      _assetRoot(assetRoot)
{
    _program->attach(GLSL_VERTEX_SHADER(
        layout(location = 0) in vec3 aPos;
        layout(location = 1) in vec4 aColor;

        uniform mat4 mvp;

        out vec4 Color;

        void main() {
            gl_Position = mvp * vec4(aPos, 1.0f);
            Color = aColor;
        }));

    _program->attach(GLSL_FRAGMENT_SHADER(
        in vec4 Color;

        out vec4 FragColor;

        void main() {
            FragColor = Color;
        }));

    _program->link();
}

RenderService::~RenderService() = default;

MeshComponent RenderService::LoadMesh(
    const std::string &modelPath,
    const std::string &name)
{
    if (!std::filesystem::exists(_assetRoot / modelPath))
    {
        fmt::print("{} does not exist in asset root {}\n", modelPath, _assetRoot.string());
        return _emptyMeshComponent;
    }

    auto fullPath = std::filesystem::canonical(_assetRoot / modelPath);

    tinygltf::Model model;
    auto found = _assetPathMap.find(fullPath);
    if (found == _assetPathMap.end())
    {
        if (!std::filesystem::exists(fullPath))
        {
            fmt::print("{0} does not exist\n", fullPath.string());

            return _emptyMeshComponent;
        }

        tinygltf::TinyGLTF loader;

        fmt::print("Loading {0}\n", fullPath.string());

        std::string err, warn;

        bool result = false;
        if (fullPath.extension() == ".glb")
        {
            result = loader.LoadBinaryFromFile(&model, &err, &warn, fullPath.string());
        }
        else
        {
            result = loader.LoadASCIIFromFile(&model, &err, &warn, fullPath.string());
        }

        if (!result)
        {
            fmt::print("failed to load {0} as gltf file\n", fullPath.string());

            if (!warn.empty()) fmt::print("Warnings: {0}\n", warn);

            if (!err.empty()) fmt::print("Errors: {0}\n", err);

            return _emptyMeshComponent;
        }

        dbgModel(model);

        _assetPathMap.insert(std::make_pair(fullPath, model));
    }
    else
    {
        model = found->second; // dos this copy the model?
    }

    auto nodeIndex = NodeIndexFromName(model, name);

    auto vao = bindModel(model, nodeIndex);

    auto assetIndex = _loadedAssets.size();

    _loadedAssets.push_back(LoadedAsset({fullPath, name, vao}));

    return MeshComponent({
        vao,        // vao
        assetIndex, // asset
        nodeIndex,  // node
    });
}

int RenderService::NodeIndexFromName(
    const tinygltf::Model &model,
    const std::string &name)
{
    if (name.empty())
    {
        return -1;
    }

    auto found = std::find_if(
        model.nodes.begin(),
        model.nodes.end(),
        [&name](const tinygltf::Node &node) { return (node.name == name); });

    if (found == model.nodes.end())
    {
        return -1;
    }

    return found - model.nodes.begin();
}

bool operator==(const MeshComponent &left, const MeshComponent &right)
{
    return left.asset == right.asset && left.node == right.node && left.vao == right.vao;
}

void RenderService::RenderMesh(
    const MeshComponent &meshComponent,
    const glm::mat4 &matrix)
{
    if (meshComponent == _emptyMeshComponent)
    {
        return;
    }

    if (meshComponent.asset >= _loadedAssets.size())
    {
        return;
    }

    _program->use();

    _program->setUniformMatrix("mvp", matrix);

    const auto &asset = _loadedAssets[meshComponent.asset];
    const auto &model = _assetPathMap[asset.path];

    if (meshComponent.node >= 0)
    {
        glBindVertexArray(asset.vao);

        const auto &node = model.nodes[meshComponent.node];
        drawModelNodes(model, node);
    }
    else
    {
        drawModel(asset.vao, model);
    }
}
