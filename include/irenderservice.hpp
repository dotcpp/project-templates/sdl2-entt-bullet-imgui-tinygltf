#ifndef IRENDERSERVICE_HPP
#define IRENDERSERVICE_HPP

#include "entities.hpp"
#include <glm/glm.hpp>
#include <string>

class IRenderService
{
public:
    virtual ~IRenderService() = default;

    virtual MeshComponent LoadMesh(
        const std::string &modelPath,
        const std::string &name = "") = 0;

    virtual void RenderMesh(
        const MeshComponent &mesh,
        const glm::mat4 &matrix) = 0;
};

#endif // IRENDERSERVICE
