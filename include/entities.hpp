#ifndef ENTITIES_HPP
#define ENTITIES_HPP

#include <glad/glad.h>

struct MeshComponent
{
    GLuint vao = 0;
    size_t asset = 0;
    int node = -1;
};

struct PhysicsComponent
{
    size_t bodyIndex = 0;
};


#endif // ENTITIES_HPP
