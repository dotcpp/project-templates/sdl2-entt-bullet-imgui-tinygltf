# sdl2-entt-bullet-imgui-tinygltf

SDL2 project using IMGUI, EnTT, TinyGLTF and Bullet Physics with the latest OpenGL.

This project makes use of the following libraries/projects:

* [SDL2](https://www.libsdl.org/)
* [IMGUI](https://github.com/ocornut/imgui/)
* [GLM](https://github.com/g-truc/glm/)
* [CPM](https://github.com/cpm-cmake/CPM.cmake)
* [GLAD](https://glad.dav1d.de/)
* [EnTT](https://github.com/skypjack/entt/)
* [BULLET3](https://github.com/bulletphysics/bullet3/)
* [TINYGLTF](https://github.com/syoyo/tinygltf)

Using this project with ``dotcpp`` is a single line command:

```
dotcpp new sdl2-entt-bullet-imgui-tinygltf --output ./sdl2-entt-bullet-imgui-tinygltf
```

![Screenshot](screenshot.gif)
